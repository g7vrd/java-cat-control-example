package uk.co.g7vrd.jcatcontrol.example;


import com.fazecast.jSerialComm.SerialPort;
import uk.co.g7vrd.jcatcontrol.JCatControl;
import uk.co.g7vrd.jcatcontrol.JCatControlBuilder;
import uk.co.g7vrd.jcatcontrol.example.listener.*;
import uk.co.g7vrd.jcatcontrol.listeners.ChangeListener;
import uk.co.g7vrd.jcatcontrol.listeners.OptionalChangeListener;
import uk.co.g7vrd.jcatcontrol.operations.*;

import java.time.Instant;
import java.util.Optional;

/**
 * A simple example program
 */
public class Main {

  public static void main(String... args) {

    System.out.println("Detected comm ports:");
    final SerialPort[] commPorts = SerialPort.getCommPorts();
    for (int i = 0; i < commPorts.length; i++) {
      final SerialPort commPort = commPorts[i];
      System.out.println(i + " - " + commPort.getSystemPortName() + " " + commPort.getPortDescription() + " " + commPort.getDescriptivePortName());
    }

    if (args.length != 7) {
      System.err.println("Usage: <rigId> <serial port> <baudrate> <databits> <parity> <stopbits> <sleepms>");
      System.err.println("Example: TS590SG ttyUSB0 115200 8 NONE 1 100");
      System.exit(1);
    }

    JCatControl jCatControl = new JCatControlBuilder()
      .rigId(args[0])
      .serialPort(args[1])
      .baudRate(Integer.parseInt(args[2]))
      .dataBits(Integer.parseInt(args[3]))
      .parity(JCatControl.Parity.valueOf(args[4]))
      .stopBits(Integer.parseInt(args[5]))
      .sleepMs(Integer.parseInt(args[6]))
      .build();

    jCatControl.addChangeListener(Frequency.class, (ts, currentFrequency, frequency) ->
      System.out.println(ts + ": Frequency                  : " + ((Frequency) frequency).getMhz() + " MHz"));
    jCatControl.addChangeListener(Mode.class, (ts, currentMode, mode) ->
      System.out.println(ts + ": Mode                       : " + mode));
    jCatControl.addOptionalChangeListener(RxSignal.class, new InfrequentSignalChangeListener());
    jCatControl.addOptionalChangeListener(TxPower.class, (ts, last, current) ->
      current.ifPresent(swrMeter -> System.out.println(ts + ": TxPower                    : " + ((TxPower) current.get()).getWatts() + "W")));
    jCatControl.addChangeListener(OutputPower.class, (ts, last, current) ->
      System.out.println(ts + ": OutputPower                : " + ((OutputPower) current).getWatts() + "W"));
    jCatControl.addChangeListener(TxRxStatus.class, (ts, lastTxRxStatus, current) ->
      System.out.println(ts + ": TxRxStatus                 : " + current));
    jCatControl.addChangeListener(DataMode.class, (ts, lastDataMode, current) ->
      System.out.println(ts + ": DataMode                   : " + current));
    jCatControl.addChangeListener(HighCut.class, (ts, currentHighCut, highCut) ->
      System.out.println(ts + ": HighCut                    : " + ((HighCut) highCut).getHz() + " Hz"));
    jCatControl.addChangeListener(LowCut.class, (ts, currentLowCut, lowCut) ->
      System.out.println(ts + ": LowCut                     : " + ((LowCut) lowCut).getHz() + " Hz"));
    jCatControl.addOptionalChangeListener(SwrMeter.class, (instant, last, current) ->
      current.ifPresent(swrMeter -> System.out.println(((SwrMeter) swrMeter).getSwr())));
    jCatControl.addOptionalChangeListener(SwrMeter.class, (ts, last, current) ->
      current.ifPresent(swrMeter -> System.out.println(ts + ": SWR                        : 1:" + ((SwrMeter) current.get()).getSwr())));
    jCatControl.addOptionalChangeListener(CompressionMeter.class, (ts, last, current) ->
      current.ifPresent(compressionMeter -> System.out.println(ts + ": Compression                : " + ((CompressionMeter) current.get()).getCompression())));
    jCatControl.addOptionalChangeListener(AlcMeter.class, (ts, last, current) ->
      current.ifPresent(alcMeter -> System.out.println(ts + ": ALC                        : " + ((AlcMeter) current.get()).getAlc())));
    jCatControl.addChangeListener(InternalAntennaTunerStatus.class, (ts, last, current) -> {
      InternalAntennaTunerStatus status = (InternalAntennaTunerStatus) current;
      System.out.println(ts + ": InternalAntennaTunerStatus : rx: " + status.getRx() + ", tx:" + status.getTx() + ", tuning: " + status.getTuning());
    });
    jCatControl.addChangeListener(SelectedAntenna.class, (ts, last, current) ->
      System.out.println(ts + ": SelectedAntenna            : " + ((SelectedAntenna) current).getAntenna()));
    jCatControl.addChangeListener(BusyStatus.class, (ts, last, current) ->
      System.out.println(ts + ": Busy                       : " + current));

    jCatControl.startRig();
  }
}
