package uk.co.g7vrd.jcatcontrol.example.listener;

import uk.co.g7vrd.jcatcontrol.listeners.OptionalChangeListener;
import uk.co.g7vrd.jcatcontrol.operations.Monitorable;
import uk.co.g7vrd.jcatcontrol.operations.RxSignal;

import java.time.Instant;
import java.util.Optional;

public class InfrequentSignalChangeListener implements OptionalChangeListener<RxSignal> {

  private int num;

  @Override
  public void event(Instant now, Optional<? extends Monitorable> last, Optional<? extends Monitorable> current) {
//    num++;
//    if (num == 10) {
    current.ifPresent(rxSignal -> System.out.println(now + ": RxSignal                   : " + ((RxSignal) rxSignal).getDb() + " db"));
//      num = 0;
//    }
  }
}
